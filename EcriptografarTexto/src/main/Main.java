package main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import metodos.Decrypt;
import metodos.Encrypt;
import metodos.LerArquivo;

public class Main {	

	@SuppressWarnings("rawtypes")
	public static void main(String[] args) throws IOException {
		
		String operacao = menuEscolha();
		
		while(operacao != null ){
			 if(operacao.equals("1")) {
		         	ArrayList a = criptografar();
		         	for(int i = 0; i < a.size(); i++) {
		         		if(i == 0)
		         		System.out.println("Lista de quantidade de ocorrencias de palavras come�adas com cada letra:\n"+ a.get(i).toString());
		         		else
		         			System.out.println(a.get(i).toString());
		         	}
		         	
		         	operacao = menuEscolha();
			 }
			 else if(operacao.equals("2")) {
				 ArrayList b = decriptografar();
				 for(int i = 0; i < b.size(); i++) {
		         		if(i == 0)
		         		System.out.println("Lista de quantidade de ocorrencias de palavras come�adas com cada letra:\n"+ b.get(i).toString());
		         		else
		         			System.out.println(b.get(i).toString());
		         	}
		         	operacao = menuEscolha();
			 }
			 else {
				 System.out.printf("Voc� digitou uma opera��o inv�lida.");
	             operacao = menuEscolha();
			 }
        }
		
	}
	public static String menuEscolha() {		
		String operacao;
		@SuppressWarnings("resource")
		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Escolha 1 para Criptografar ou  2 para Descriptografar: ");
        operacao = entrada.nextLine();
;		
//		System.out.println("Entre com o diretorio do arquivo: ");
//		BufferedReader diretorio = new BufferedReader(new InputStreamReader(System.in)); 
//		File file = new File ("C:/Users/Luiz/Documents/TesteCriptografia/WarAndPeace-LeoTolstoy.txt");
//		File fileUrlDecrypt = new File ("C:/Users/Luiz/git/Nova pasta/EcriptografarTexto/TESTE.txt");
//		Map<String, Object> resposta = ler.lerArquivo(file);
//		Encrypt.encryptFile(chave, file.toString(), fileName);
//		Decrypt.decryptFile(chave, fileUrlDecrypt.toString(), arquivoDescriptografado);
		return operacao;
	}
	
	@SuppressWarnings({ "resource", "rawtypes", "unchecked" })
	public static ArrayList criptografar() throws IOException {
		LerArquivo ler = new LerArquivo();		
		String chave;
		String fileName;

		String caminho;
				
		Scanner entrada = new Scanner(System.in);
        System.out.println("Entre com o caminho do arquivo a ser criptografado:");
        caminho = entrada.nextLine();
        System.out.println("Digite o caminho do novo arquivo criptografado:");
        fileName = entrada.nextLine();
        System.out.println("Digite uma chave para criptografar:");
        chave = entrada.nextLine();
        ArrayList resposta = ler.lerArquivo(new File(caminho));
        String res = Encrypt.encryptFile(chave, caminho, fileName);
        resposta.add(res);
        
        return resposta;
	}
	
	@SuppressWarnings({ "resource", "unchecked", "rawtypes" })
	public static ArrayList decriptografar() throws IOException {
		LerArquivo ler = new LerArquivo();
		
		String chave;
		String fileName;
		String caminho;
				
		Scanner entrada = new Scanner(System.in);
        System.out.println("Entre com o caminho do arquivo a ser descriptografado:");
        caminho = entrada.nextLine();
        System.out.println("Digite o caminho do novo arquivo:");
        fileName = entrada.nextLine();
        System.out.println("Digite a chave usada para criptografar:");
        chave = entrada.nextLine();
        ArrayList resposta = ler.lerArquivo(new File(caminho));
        String res = Decrypt.decryptFile(chave, caminho, fileName);
        resposta.add(res);
        return resposta;
	} 

}
