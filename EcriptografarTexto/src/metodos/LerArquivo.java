package metodos;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class LerArquivo {
	
	
	@SuppressWarnings({ "unchecked", "rawtypes", "resource" })
	public ArrayList lerArquivo(File file) throws FileNotFoundException {
		ArrayList list = new ArrayList();
		HashMap resposta = new HashMap();
		Scanner ler = new Scanner(file);
		int a = 0; int b = 0; int c = 0; int d = 0; int e = 0 ;
		int f = 0; int g = 0; int h = 0; int i = 0; int j = 0 ;
		int k = 0; int l = 0; int m = 0; int n = 0; int o = 0 ;
		int p = 0; int q = 0; int r = 0; int s = 0; int t = 0 ;
		int u = 0; int v = 0; int w = 0; int x = 0; int y =0; int z = 0; int desconhecido = 0;
	    
	    while(ler.hasNextLine()){
		    String nome = ler.nextLine();
		    String textLowerCase = nome.toLowerCase();
		    
		    if(!textLowerCase.equals("")) {
		    	 String[] palavras = textLowerCase.split("[\\s]");
			 	    
			 	    String palavra;
			 		for(int ii = 0; ii <= palavras.length; ii++) {
			 			try {
				 			if(palavras[ii] != null) {
				 				palavra = palavras[ii];				 			
					 			
					 			if(palavra.charAt(0) == 'a')
					 	    		a += 1;
					 	    	else if(palavra.charAt(0) == 'b')
					 	    		b += 1;
					 	    	else if(palavra.charAt(0) == 'c')
					 	    		c += 1;
					 	    	else if(palavra.charAt(0) == 'd')
					 	    		d += 1;
					 	    	else if(palavra.charAt(0) == 'e')
					 	    		e += 1;
					 	    	else if(palavra.charAt(0) == 'f')
					 	    		f += 1;
					 	    	else if(palavra.charAt(0) == 'g')
					 	    		g += 1;
					 	    	else if(palavra.charAt(0) == 'h')
					 	    		h += 1;
					 	    	else if(palavra.charAt(0) == 'i')
					 	    		i += 1;
					 	    	else if(palavra.charAt(0) == 'j')
					 	    		j += 1;
					 	    	else if(palavra.charAt(0) == 'k')
					 	    		k += 1;
					 	    	else if(palavra.charAt(0) == 'l')
					 	    		l += 1;
					 	    	else if(palavra.charAt(0) == 'm')
					 	    		m += 1;
					 	    	else if(palavra.charAt(0) == 'n')
					 	    		n += 1;
					 	    	else if(palavra.charAt(0) == 'o')
					 	    		o += 1;
					 	    	else if(palavra.charAt(0) == 'p')
					 	    		p += 1;
					 	    	else if(palavra.charAt(0) == 'q')
					 	    		q += 1;
					 	    	else if(palavra.charAt(0) == 'r')
					 	    		r += 1;
					 	    	else if(palavra.charAt(0) == 's')
					 	    		s += 1;
					 	    	else if(palavra.charAt(0) == 't')
					 	    		t += 1;
					 	    	else if(palavra.charAt(0) == 'u')
					 	    		u += 1;
					 	    	else if(palavra.charAt(0) == 'v')
					 	    		v += 1;
					 	    	else if(palavra.charAt(0) == 'x')
					 	    		x += 1;
					 	    	else if(palavra.charAt(0) == 'w')
					 	    		w+= 1;
					 	    	else if(palavra.charAt(0) == 'y')
					 	    		y += 1;
					 	    	else if(palavra.charAt(0) == 'z')
					 	    		z += 1;
					 	    	else
					 	    		desconhecido += 1;
				 			}
						} catch (Exception e2) {
							
						}

			 			
			 	    }
		    }	    	
	    }
	    
	   
	    resposta.put("A", a);
	    resposta.put("B", b);
	    resposta.put("C", c);
	    resposta.put("D", d);
	    resposta.put("E", e);
	    resposta.put("F", f);
	    resposta.put("G", g);
	    resposta.put("H", h);
	    resposta.put("I", i);
	    resposta.put("J", j);
	    resposta.put("K", k);
	    resposta.put("L", l);
	    resposta.put("M", m);
	    resposta.put("N", n);
	    resposta.put("O", o);
	    resposta.put("P", p);
	    resposta.put("Q", q);
	    resposta.put("R", r);
	    resposta.put("S", s);
	    resposta.put("T", t);
	    resposta.put("U", u);
	    resposta.put("V", v);
	    resposta.put("W", w);
	    resposta.put("X", x);
	    resposta.put("Y", y);
	    resposta.put("Z", z);
	    resposta.put("Caracter desconhecido", desconhecido);
	    
	    list.add(resposta);
	    return list;
	}
}
